api = 2
core = 7.x
; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make
projects[openlist][download][type] = "git"
projects[openlist][download][profile] = "contributions/profiles/openlist"
projects[openlist][download][revision] = "7.x-1.x"