<?php
/**
 * @file
 * openlist_listing.features.inc
 */

/**
 * Implements hook_node_info().
 */
function openlist_listing_node_info() {
  $items = array(
    'openlist_item' => array(
      'name' => t('List Item'),
      'base' => 'node_content',
      'description' => t('A list item could be a range of things, a car or home listing, an e-commerce product, a book title, a news article.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
