<?php

/**
 * Update installation defaults 
 */
function _openlist_install_defaults() {
  
  /**
   * Create custom blocks for this site
   */
  
  
  //_openlist_load_fusion_styles();
 
  
  /**
   * Add menu links manually
   */
  //_openlist_add_menu_links();
  
  
  /**
   * Update wysiwyg settings
   */
  
  db_insert('wysiwyg')
  ->fields(array(
    'format' => 'full_html',
    'editor' => 'tinymce',
    'settings' => 'a:20:{s:7:"default";i:1;s:11:"user_choose";i:0;s:11:"show_toggle";i:1;s:5:"theme";s:8:"advanced";s:8:"language";s:2:"en";s:7:"buttons";a:7:{s:7:"default";a:21:{s:4:"bold";i:1;s:6:"italic";i:1;s:9:"underline";i:1;s:13:"strikethrough";i:1;s:11:"justifyleft";i:1;s:13:"justifycenter";i:1;s:12:"justifyright";i:1;s:11:"justifyfull";i:1;s:7:"bullist";i:1;s:7:"numlist";i:1;s:7:"outdent";i:1;s:6:"indent";i:1;s:4:"undo";i:1;s:4:"redo";i:1;s:4:"link";i:1;s:6:"unlink";i:1;s:6:"anchor";i:1;s:5:"image";i:1;s:9:"forecolor";i:1;s:9:"backcolor";i:1;s:4:"code";i:1;}s:4:"font";a:1:{s:12:"formatselect";i:1;}s:10:"fullscreen";a:1:{s:10:"fullscreen";i:1;}s:5:"paste";a:2:{s:9:"pastetext";i:1;s:9:"pasteword";i:1;}s:4:"imce";a:1:{s:4:"imce";i:1;}s:6:"iframe";a:1:{s:6:"iframe";i:1;}s:6:"drupal";a:1:{s:5:"media";i:1;}}s:11:"toolbar_loc";s:3:"top";s:13:"toolbar_align";s:4:"left";s:8:"path_loc";s:6:"bottom";s:8:"resizing";i:1;s:11:"verify_html";i:1;s:12:"preformatted";i:0;s:22:"convert_fonts_to_spans";i:1;s:17:"remove_linebreaks";i:1;s:23:"apply_source_formatting";i:1;s:27:"paste_auto_cleanup_on_paste";i:1;s:13:"block_formats";s:32:"p,address,pre,h2,h3,h4,h5,h6,div";s:11:"css_setting";s:4:"none";s:8:"css_path";s:0:"";s:11:"css_classes";s:0:"";}',
  ))
  ->execute();
  
  /**
   * Update IMCE settings
   * 
   */
  db_insert('variable')
  ->fields(array(
    'name' => 'imce_roles_profiles',
    'value' => 'a:4:{i:4;a:4:{s:6:"weight";s:1:"0";s:9:"vimeo_pid";s:1:"1";s:11:"youtube_pid";s:1:"1";s:10:"public_pid";s:1:"1";}i:3;a:4:{s:6:"weight";s:1:"0";s:9:"vimeo_pid";i:0;s:11:"youtube_pid";i:0;s:10:"public_pid";i:0;}i:2;a:4:{s:6:"weight";i:11;s:9:"vimeo_pid";i:0;s:11:"youtube_pid";i:0;s:10:"public_pid";i:0;}i:1;a:4:{s:6:"weight";i:12;s:9:"vimeo_pid";i:0;s:11:"youtube_pid";i:0;s:10:"public_pid";i:0;}}',
  ))
  ->execute();
  
  
  /**
   * Display settings for content types (e.g. enabling RSS view mode for podcast)
   */
  db_update('variable')
  ->fields(array(
    'value' => 'a:2:{s:4:"file";a:4:{s:7:"default";a:2:{s:10:"view_modes";a:0:{}s:12:"extra_fields";a:2:{s:4:"form";a:0:{}s:7:"display";a:1:{s:4:"file";a:1:{s:11:"media_small";a:2:{s:6:"weight";i:0;s:7:"visible";b:0;}}}}}s:5:"image";a:2:{s:10:"view_modes";a:0:{}s:12:"extra_fields";a:2:{s:4:"form";a:0:{}s:7:"display";a:1:{s:4:"file";a:1:{s:11:"media_small";a:2:{s:6:"weight";i:0;s:7:"visible";b:0;}}}}}s:5:"audio";a:2:{s:10:"view_modes";a:0:{}s:12:"extra_fields";a:2:{s:4:"form";a:0:{}s:7:"display";a:1:{s:4:"file";a:1:{s:11:"media_small";a:2:{s:6:"weight";i:0;s:7:"visible";b:0;}}}}}s:5:"video";a:2:{s:10:"view_modes";a:0:{}s:12:"extra_fields";a:2:{s:4:"form";a:0:{}s:7:"display";a:1:{s:4:"file";a:1:{s:11:"media_small";a:2:{s:6:"weight";i:0;s:7:"visible";b:0;}}}}}}s:4:"node";a:1:{s:18:"openlist_podcast";a:2:{s:10:"view_modes";a:6:{s:6:"teaser";a:1:{s:15:"custom_settings";b:1;}s:3:"rss";a:1:{s:15:"custom_settings";b:1;}s:4:"full";a:1:{s:15:"custom_settings";b:0;}s:12:"search_index";a:1:{s:15:"custom_settings";b:0;}s:13:"search_result";a:1:{s:15:"custom_settings";b:0;}s:5:"token";a:1:{s:15:"custom_settings";b:0;}}s:12:"extra_fields";a:2:{s:4:"form";a:0:{}s:7:"display";a:0:{}}}}}',
  ))
  ->condition('name', 'field_bundle_settings')
  ->execute();
}

/**
 * Load fusion styles
 */
function _openlist_load_fusion_styles(){
  
  /**
   * Load fusion skin info for site blocks and save
   */
  $xml = simplexml_load_file('includes/openlist_theme.xml');

  $json = json_encode($xml);
  $array = json_decode($json, TRUE);

  foreach($array['database']['table_data']['row'] as $row){
    $row = $row['field'];

    $skin = array(
      'theme' => 'openlist_theme',
      'module' => $row[2],
      'element' => $row[3],
      'skin' => $row[4],
      'options' => unserialize($row[5]),
      'status' => $row[6],
    );

    drupal_write_record('fusion_apply_skins', $skin);
  }
}
  

/**
 * Add block title
 */
function _openlist_add_block_title($bid, $title){
  
  foreach (array('openlist_theme') as $theme){
    $settings = new stdClass;
    $settings->module = 'block';
    $settings->delta = $bid;
    $settings->theme = $theme;
    $settings->status = 0;
    $settings->weight = 0;
    $settings->region = -1;
    $settings->custom = 0;
    $settings->visibility = 0;
    $settings->pages = '';
    $settings->title = $title;
    $settings->cache -1;

    $block = (array) $settings;

    drupal_write_record('block', $block);
  }
}

/**
 * Add menu links
 */
function _openlist_add_menu_links(){
}